/*Angular Module */

var app = angular.module('portfolioApp', []);

app.controller('portfolioController', function($scope) {
  $scope.projects = projects;
  $scope.education = education;
  $scope.detailShow = false;
  var section = angular.element("section");
  var details = angular.element(".details");
  var navBar = angular.element(".myNavBar")


  $scope.showDetails = function(myProj) {
    $scope.currentProject = myProj;
  };

  $scope.hideDetails = function() {
    $scope.detailShow = false;
  };
  /*Jquery Functions*/
  $(window).scroll(function() {
    var nav = $(".myNavBar");
    var navLink = $(".myNavBar a");
    var navSpan = $(".myNavBar span");
    var navHover = $(".myNavBar a:hover")
    var offset = $("body").scrollTop();
    if (offset != 0) {
      nav.css("background-color", "white");
      navLink.css("color", "white");
      navLink.css("color", "black");
      navSpan.css("background-color", "black");
    } else {
      nav.css("background-color", "black");
      navLink.css("color", "white");
      navSpan.css("background-color", "white");
    }
  });


  $(".menuAnchor").click(function(event) {
    event.preventDefault();
    var ref = $(this).attr("href");

    $(window).scrollTop($(ref).offset().top - (40));
  });
});

$(document).on('click', '.navbar-collapse.in', function(e) {
  if ($(e.target).is('a')) {
    $(this).collapse('hide');
  }
});

/*Global Variables Declaration */

var projects = [{
    "name": "FreeCodeCamp.org",
    "date": "Summer 2017",
    "description": "Solved different algorithmic problems using JS, JSON APIs, and Ajax. Developed small projects such as JS calculator, Pomodoro clock, Tic Tac Toe.",
    "technologies": "HTML, CSS, JS, Jquery, AJAX",
    "image": "images/freecodecamp_thumbnail.png",
    "descriptionimage": "images/freecodecamp.png",
    "sourceCode": "codepen.io",
    "link": "https://www.freecodecamp.org/alexmonroy",
  },
  {
    "name": "Mira Augmented Reality fighting game",
    "date": "Summer 2017",
    "description": "Developed an augmented reality fighting game to test the alpha version of Mira Prism augmented reality headset.",
    "tasks": ["Develop game using Unity3D game engine", "Game testing and debugging on iphone 7", "Programming Mira Prism controller functions for fighting"],
    "technologies": "Unity3D, Mira Prisma Headset",
    "image": "images/mira_thumbnail.png",
    "descriptionimage": "images/miraGame.jpg",
    "sourceCode": "Game Repository",
    "link": "https://bitbucket.org/monroyrea/miragamebattle",
    "video": "https://www.youtube.com/watch?v=Pn5zF3_I4Xs"
  },
  {
    "name": "Facebook search responsive design",
    "date": "Spring 2017",
    "description": "A responsive web page that allows the user to fetch data from FB Graph API related to users, pages, events, places, groups, photos and posts related to every page. The app uses HTML5 local storage and location to allow the user to add data to favorites and search information depending on its site.",
    "technologies": "HTML, CSS, PHP, AngularJS, Facebook API",
    "image": "images/responsive_thumbnail.png",
    "descriptionimage": "images/angular_project.png",
    "sourceCode": "Facebook search responsive",
    "link": "https://bitbucket.org/monroyrea/responsive_facebooksearch"
  },
  {
    "name": "Facebook search mobile",
    "date": "Spring 2017",
    "description": "An Android App that allows the user to fetch data from Facebook Grap API related to users, pages, events, places, groups, photos and posts related to every page. Concepts such as activities, fragments, intents, HTTP requests, listviews, navigation drawer, among others, were used.",
    "technologies": "Android, PHP, Facebook API",
    "image": "images/mobile_thumbnail.png",
    "descriptionimage": "images/android_project.png",
    "sourceCode": "Facebook search mobile",
    "link": "https://bitbucket.org/monroyrea/android_fbsearch"
  },
  {
    "name": "Augmented reality system to support machinery operation",
    "date": "Fall 2016",
    "description": "This paper proposes a mobile augmented reality (MAR) system aimed to support students in the use of a milling and lathe machines at a university manufacturing laboratory. The system incorporates 3D models of machinery and tools, text instructions, animations and videos with real processes to enrich the information obtained from the real world.",
    "technologies": "Augmented Reality, Unity3D, C# scripting",
    "image": "images/ar_thumbnail.png",
    "descriptionimage": "images/augmented_project.jpg",
    "sourceCode": "pending",
  },
  {
    "name": "Object Oriented Programming",
    "date": "Fall 2016",
    "description": "Fundamentals of computer programming, object-oriented programming using Java and C++ languages.",
    "technologies": "Java, C, C++",
    "image": "images/java_thumbnail.jpg",
    "descriptionimage": "images/java_projects.png",
    "sourceCode": "pa1 coin toss simulator,pa2 polynomial calculator, pa3 labrynt path search, pa4 random text gen, c++",
    "link": "https://bitbucket.org/monroyrea/"
  },
  {
    "name": "Robert Bosch full time inventory analyst and programmer",
    "date": "2015 - Spring 2016",
    "description": "I Worked as an intern for 11 months and one year as a full-time employee. Spent half of my time developing C# applications for report automation and database applications for inventory management. Moreover, as an inventory analyst, I developed root cause analysis (RCA) for inventory problems.",
    "tasks": ["Developed macros and C# applications for report automation", "Developed database systems for inventory control", "Root cause analysys (RCA) for inventory problems in manufacturing area"],
    "technologies": "Visual Studio, C#, Excel VBA, Databases",
    "image": "images/bosch_thumbnail.jpg",
    "descriptionimage": "images/bosch1.png",
    "sourceCode": "Not available due to company policies"
  }
];


var education = {
  "school": [{
      "name": "University of Southern California",
      "dpt": "Viterbi School of Engineering",
      "major": "Master of Science in Computer Science",
      "location": "Los Angeles, CA.",
      "date": "2016-2018",
      "image": "images/usc.png",
      "coursework": [{
          "name": "CSCI 455X: Introduction to programming system design",
          "content": "Fundamentals of computer programming, object-oriented programming using Java and C++ languages."
        },
        {
          "name": "CSCI 585: Database Systems",
          "content": "SQL, intro. to spatial databases, intro. to NoSQL databases."
        },
        {
          "name": "CSCI 571: Web Technologies",
          "content": "HTML and CSS, Javascript, Web Servers, PHP, AngularJS, intro to AWS and Google Cloud"
        },
        {
          "name": "CSCI 402: Introduction to Operating Systems",
          "content": "C language programming, Multithreading programming, Filesystems and Virtual Memory"
        },
        {
          "name": "CSCI 526: Advanced Mobile Devices and Game Consoles",
          "content": "Unity3D, Game development basic techniques"
        }
      ]
    },
    {
      "name": "Universidad Autonoma de Ciudad Juarez",
      "dpt": "Dept of Engineering and Technology",
      "major": "Mechatronics Engineering",
      "location": "Chihuahua, Mexico.",
      "date": "2010-2015",
      "image": "images/uacj.png",
      "coursework": [{
          "name": "Industrial Automation",
          "content": "PLC allen bradley."
        },
        {
          "name": "Robotics",
          "content": "Intro to mobile robotics, C++ programming"
        },
        {
          "name": "Electronics, sensors and control theory",
          "content": "Analogue and digital electronics."
        },
        {
          "name": "Software Programming",
          "content": "C, C++, Arduino"
        }
      ]
    }
  ]
}
